package com.assignment.carparkingsystem.service;

import com.assignment.carparkingsystem.dto.CarParkDto;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.model.CarPark;
import java.util.List;

public interface CarParkService {

    /**
     * Computes the nearest car parks from your coordinate.
     * <p>
     * This method returns CarParkDtoList object that contains two fields, carParks which is list of
     * car parks, and totalCarParks, which consists of total car parks received.
     *
     * @param carParkQueryDto consists of all the request parameters.
     * @return the result an CarParkDtoList.
     */
    CarParkDtoList findNearestCarParks(CarParkQueryDto carParkQueryDto);

    /**
     * Returns all car parks from your coordinate.
     * <p>
     * This method returns List of CarParkDto object which consists of all the car park details.
     *
     * @return the result is List of CarParkDto.
     */
    List<CarParkDto> findAllCarParks();

    /**
     * Saves or updates all car parks.
     *
     * @param carParks consists of all the car parks to be inserted or updated.
     */
    void saveAllCarParks(List<CarPark> carParks);

    /**
     * Returns a car park consisting of the specified car park number.
     *
     * @param carParkNumber consists of the car park number or identifier.
     * @return the result is the updated car park detail.
     */
    CarPark findByCarParkNumber(String carParkNumber);
}