package com.assignment.carparkingsystem.service;

import com.assignment.carparkingsystem.constants.ExceptionMessages;
import com.assignment.carparkingsystem.dto.KafkaCarParksEventDTO;
import com.assignment.carparkingsystem.model.CarPark;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumerServiceImpl implements KafkaConsumerService {

    private final CarParkService carParkService;

    @Override
    @KafkaListener(topics = {"${com.assignment.carparkingsystem.kafka.topic}"},
        containerFactory = "kafkaListenerContainerFactory", groupId = "car-parking")
    public void consumeUpdateCarParkEvent(ConsumerRecord<String, String> consumerRecord,
        Acknowledgment acknowledgment) {
        try {
            log.info("consumeCarParkLotUpdatedEvent received.");
            List<CarPark> carParks = new ArrayList<>();

            // Parse kafka string to kafka dto
            String kafkaEventDTOString = consumerRecord.value();
            KafkaCarParksEventDTO kafkaCarParksEventDTO = new ObjectMapper().readValue(
                kafkaEventDTOString, KafkaCarParksEventDTO.class);

            log.info("successfully parsed string to KafkaCarParksEventDto class object.");

            kafkaCarParksEventDTO.getItems()
                .forEach(item -> item.getCarParkData().forEach(carParkData -> {
                    int availableLots = 0;
                    int totalLots = 0;
                    // calculate all available and total lots by adding the value for all lot types
                    for (KafkaCarParksEventDTO.CarParkInfo carParkInfo : carParkData.getCarParkInfo()) {
                        availableLots += carParkInfo.getLotsAvailable();
                        totalLots += carParkInfo.getTotalLots();
                    }

                    CarPark carPark = updateCarParkDetails(carParkData.getCarParkNumber(),
                        totalLots, availableLots, carParkData.getUpdateDatetime());
                    if (carPark != null) {
                        carParks.add(carPark);
                    }
                }));

            carParkService.saveAllCarParks(carParks);
            log.info("successfully updated car park data.");
        } catch (Exception ex) {
            log.error(String.format(ExceptionMessages.UPDATE_CAR_PARK_ERROR, ex));
        }
    }

    private CarPark updateCarParkDetails(String carParkNumber, int totalLots, int availableLots,
        String updateDatetime) {
        CarPark carPark = carParkService.findByCarParkNumber(carParkNumber);
        if (carPark != null) {
            carPark.setAvailableLots(availableLots);
            carPark.setTotalLots(totalLots);
            carPark.setUpdatedAt(LocalDateTime.parse(updateDatetime));
            return carPark;
        } else {
            log.error(String.format(ExceptionMessages.CAR_PARK_NUMBER_NOT_FOUND, carParkNumber));
        }
        return null;
    }
}
