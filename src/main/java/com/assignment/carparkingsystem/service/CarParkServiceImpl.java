package com.assignment.carparkingsystem.service;

import com.assignment.carparkingsystem.dto.CarParkDto;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.repository.CarParkRepository;
import com.assignment.carparkingsystem.util.CoordinateDistanceCalculator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarParkServiceImpl implements CarParkService {

    private final CarParkRepository carParkRepository;
    private final CoordinateDistanceCalculator coordinateDistanceCalculator;

    @Override
    public CarParkDtoList findNearestCarParks(CarParkQueryDto carParkQueryDto) {

        List<CarPark> carParksList = carParkRepository.findByAvailableLotsNot(0);
        if (carParksList.isEmpty()) {
            return new CarParkDtoList(new ArrayList<>(), 0);
        }
        log.info("Received all car parks with available lots");
        List<CarParkDto> carParkDtoList = new ArrayList<>();
        carParksList.forEach(carPark -> carPark.setDistance(
            coordinateDistanceCalculator.calculateHaversineDistance(carParkQueryDto.getLatitude(),
                carParkQueryDto.getLongitude(), carPark.getLatitude(), carPark.getLongitude())));

        //sort based in distance and limit result based on pagination fields
        int skipCount = (carParkQueryDto.getPage() - 1) * carParkQueryDto.getPerPage();
        carParksList.stream().sorted(Comparator.comparingDouble(CarPark::getDistance))
            .skip(skipCount)
            .limit(carParkQueryDto.getPerPage())
            .forEach(carPark -> {
                //set latitude and longitude place values limit to 6
                carPark.setLatitude(
                    Double.parseDouble(String.format("%.6g", carPark.getLatitude())));
                carPark.setLongitude(
                    Double.parseDouble(String.format("%.6g", carPark.getLongitude())));
                carParkDtoList.add(new CarParkDto(carPark));
            });

        log.info("Received sorted and paginated car parks");
        return new CarParkDtoList(carParkDtoList, carParksList.size());
    }

    @Override
    public List<CarParkDto> findAllCarParks() {
        List<CarParkDto> carParkDtoList = new ArrayList<>();
        carParkRepository.findAll().forEach(carPark -> carParkDtoList.add(new CarParkDto(carPark)));
        return carParkDtoList;
    }

    @Override
    public void saveAllCarParks(List<CarPark> carParks) {
        carParkRepository.saveAll(carParks);
    }

    @Override
    public CarPark findByCarParkNumber(String carParkNumber) {
        return carParkRepository.findOneByCarParkNo(carParkNumber);
    }
}
