package com.assignment.carparkingsystem.gps;

import org.springframework.stereotype.Component;

@Component
public class HaversineDistanceCalculator {

    // Radius of Earth in
    // Kilometers, EARTH_RADIUS = 6371
    // Use EARTH_RADIUS = 3956 for miles
    private static final double EARTH_RADIUS = 3956;

    double haversine(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }

    public double calculateDistance(double startLat, double startLong, double endLat,
        double endLong) {

        double dLat = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat = Math.toRadians(endLat);

        double a = haversine(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversine(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c;
    }
}
