package com.assignment.carparkingsystem.exceptions;

public class UploadSeedDataException extends RuntimeException{

    public UploadSeedDataException(String message) {
        super(message);
    }

    public UploadSeedDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
