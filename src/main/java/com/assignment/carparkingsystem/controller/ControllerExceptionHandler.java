package com.assignment.carparkingsystem.controller;

import com.assignment.carparkingsystem.constants.ErrorCode;
import com.assignment.carparkingsystem.constants.ExceptionMessages;
import com.assignment.carparkingsystem.dto.BaseErrorDto;
import com.assignment.carparkingsystem.exceptions.UploadSeedDataException;
import com.fasterxml.jackson.databind.ser.Serializers.Base;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(value = {BindException.class})
    protected ResponseEntity<List<BaseErrorDto>> handleBindException(
        BindException ex) {
        List<BaseErrorDto> errors = nestedExceptionHandler(ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }

    private List<BaseErrorDto> nestedExceptionHandler(BindException exception) {
        String illegalArgumentException = "nested exception is java.lang.IllegalArgumentException";
        String numberFormatException = "nested exception is java.lang.NumberFormatException";

        return exception.getBindingResult().getFieldErrors().stream()
            .map(fieldError -> {
                String defaultMessage = fieldError.getDefaultMessage();
                if (defaultMessage.contains(illegalArgumentException)) {
                    String exceptionMessage = defaultMessage.split(
                        illegalArgumentException)[1].trim();
                    exceptionMessage = exceptionMessage.substring(2);
                    return new BaseErrorDto(ErrorCode.ILLEGAL_ARGUMENT, exceptionMessage);
                } else if (defaultMessage.contains(numberFormatException)) {
                    String exceptionMessage = String.format(
                        ExceptionMessages.NUMBER_FORMAT_EXCEPTION, fieldError.getField());
                    return new BaseErrorDto(ErrorCode.ILLEGAL_ARGUMENT, exceptionMessage);
                }
                return new BaseErrorDto(ErrorCode.ILLEGAL_ARGUMENT, defaultMessage);
            }).collect(Collectors.toList());
    }
}
