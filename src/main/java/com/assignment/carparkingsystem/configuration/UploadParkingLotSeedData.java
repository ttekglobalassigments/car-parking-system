package com.assignment.carparkingsystem.configuration;

import com.assignment.carparkingsystem.constants.ExceptionMessages;
import com.assignment.carparkingsystem.exceptions.UploadSeedDataException;
import com.assignment.carparkingsystem.gps.LatLonCoordinate;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.service.CarParkService;
import com.assignment.carparkingsystem.service.CarParkServiceImpl;
import com.assignment.carparkingsystem.util.CoordinateConvertor;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class UploadParkingLotSeedData {

    private final CarParkService carParkService;
    private final CoordinateConvertor coordinateConvertor;
    private static final String COLUMN_HEADER = "car_park_no,address,x_coord,y_coord,car_park_type,type_of_parking_system,short_term_parking,free_parking,night_parking,car_park_decks,gantry_height,car_park_basement";

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        loadParkingLotsSeedData();
    }

    private void loadParkingLotsSeedData() {

        // check if parking lots data already uploaded
        if (!carParkService.findAllCarParks().isEmpty()) {
            log.info("Parking Lots seed data already uploaded.");
            return;
        }

        // Specify the path to the CSV file containing car park data
        ClassLoader classLoader = CarParkServiceImpl.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("HDBCarparkInformation.csv");
        if (inputStream == null) {
            log.error(ExceptionMessages.INPUT_STEAM_ERROR);
            throw new UploadSeedDataException(ExceptionMessages.INPUT_STEAM_ERROR);
        }

        // List to store car park objects
        List<CarPark> carParks = new ArrayList<>();
        // Load car park data from CSV file
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                // avoid parsing row header in csv file
                if (!line.equalsIgnoreCase(COLUMN_HEADER)) {
                    // parse data from line
                    CarPark carPark = getCarPark(line);
                    carParks.add(carPark);
                }
            }
            log.info("Successfully parsed csv file data.");
        } catch (Exception e) {
            log.error(String.format(ExceptionMessages.DATA_PARSING_ERROR, e));
            throw new UploadSeedDataException(
                String.format(ExceptionMessages.DATA_PARSING_ERROR, e));
        }

        carParkService.saveAllCarParks(carParks);
    }

    private CarPark getCarPark(String line) {
        // Split CSV line by comma
        String[] parts = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

        // Extract relevant information
        String carParkName = parts[0];
        String address = parts[1].replace("\"", "");
        // Convert coordinates from SVY21 to latitude and longitude
        LatLonCoordinate latLonCoordinate = coordinateConvertor.convertSVY21ToLatLon(
            Double.parseDouble(parts[2]),
            Double.parseDouble(parts[3]));

        // Create CarPark object and add to list
        return new CarPark(carParkName, address, latLonCoordinate);
    }
}
