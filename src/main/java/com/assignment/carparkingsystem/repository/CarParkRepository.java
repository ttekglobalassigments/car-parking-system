package com.assignment.carparkingsystem.repository;

import com.assignment.carparkingsystem.model.CarPark;
import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkRepository extends CrudRepository<CarPark, UUID> {

    List<CarPark> findAll();
    CarPark findOneByCarParkNo(String carParkNo);
    List<CarPark> findByAvailableLotsNot(int availableSlots);
}
