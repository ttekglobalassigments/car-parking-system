package com.assignment.carparkingsystem.util;

import com.assignment.carparkingsystem.gps.LatLonCoordinate;
import com.assignment.carparkingsystem.gps.SVY21Coordinate;
import org.springframework.stereotype.Component;

@Component
public class CoordinateConvertor {

    /**
     * Computes SVY21 Northing and Easting based on a Latitude and Longitude coordinate.
     * <p>
     * This method returns an immutable SVY21Coordinate object that contains two fields, northing,
     * accessible with .getNorthing(), and easting, accessible with .getEasting().
     * <p>
     * This method is a shorthand for the functionally identical public SVY21Coordinate
     * computeSVY21(double lat, double lon).
     *
     * @param xCoord the x coordinate of the location.
     * @param yCoord the y coordinate of the location.
     * @return the conversion result an SVY21Coordinate.
     */
    public LatLonCoordinate convertSVY21ToLatLon(double xCoord, double yCoord) {
        SVY21Coordinate svy21Coordinate = new SVY21Coordinate(xCoord, yCoord);
        return svy21Coordinate.asLatLon();
    }
}
