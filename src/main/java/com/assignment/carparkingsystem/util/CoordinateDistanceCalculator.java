package com.assignment.carparkingsystem.util;

import com.assignment.carparkingsystem.gps.HaversineDistanceCalculator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CoordinateDistanceCalculator {

    private final HaversineDistanceCalculator haversineDistanceCalculator;

    /**
     * Caculate distance based on Haversine Formula.
     * <p>
     * This method returns a double identifying the distance between startLast, startLong, endLat,
     * endLong.
     *
     * @param startLat  starting latitude coordinate in double
     * @param startLong starting longitude coordinate in double
     * @param endLat    ending latitude coordinate in double
     * @param endLong   ending longitude coordinate in double
     * @return the calculator results the distance in double.
     */
    public double calculateHaversineDistance(double startLat, double startLong, double endLat,
        double endLong) {
        return haversineDistanceCalculator.calculateDistance(startLat, startLong, endLat, endLong);
    }

}
