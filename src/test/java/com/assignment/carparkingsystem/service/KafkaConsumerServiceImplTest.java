package com.assignment.carparkingsystem.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.dto.KafkaCarParksEventDTO;
import com.assignment.carparkingsystem.dto.KafkaCarParksEventDTO.CarParkData;
import com.assignment.carparkingsystem.dto.KafkaCarParksEventDTO.CarParkInfo;
import com.assignment.carparkingsystem.dto.KafkaCarParksEventDTO.Item;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.repository.CarParkRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KafkaConsumerServiceImplTest {

    private KafkaConsumerServiceImpl kafkaConsumerServiceImpl;
    @Mock
    private Acknowledgment acknowledgment;

    @Mock
    private ConsumerRecord consumerRecord;

    @Mock
    private CarParkService carParkService;

    KafkaCarParksEventDTO kafkaCarParksEventDTO = new KafkaCarParksEventDTO();
    CarPark carPark = new CarPark();

    @BeforeEach
    void setUp() {
        kafkaConsumerServiceImpl = new KafkaConsumerServiceImpl(carParkService);
        List<KafkaCarParksEventDTO.CarParkInfo> carParkInfos = new ArrayList<>();
        carParkInfos.add(new CarParkInfo(2, "C", 1));
        List<KafkaCarParksEventDTO.CarParkData> carParkData = new ArrayList<>();
        carParkData.add(new CarParkData("ASK", LocalDateTime.now().toString(), carParkInfos));
        List<KafkaCarParksEventDTO.Item> items = new ArrayList<>();
        items.add(new Item(LocalDateTime.now().toString(), carParkData));
        KafkaCarParksEventDTO kafkaCarParksEventDTO = new KafkaCarParksEventDTO(new ArrayList<>());
        kafkaCarParksEventDTO.setItems(items);
        carPark = new CarPark(UUID.randomUUID(), "Test", "test", 1.0, 1.0, 1, 5,
            LocalDateTime.now(), LocalDateTime.now(), 1.0);
    }

    @Test
    void testConsumeCarParkLotUpdatedEvent_When_EventDTOIsValid_Then_CallExpectedMethods() {
        doReturn(carPark).when(carParkService).findByCarParkNumber(any());
        doNothing().when(carParkService).saveAllCarParks(any());
        kafkaConsumerServiceImpl.consumeUpdateCarParkEvent(consumerRecord, acknowledgment);
        verify(carParkService, times(0)).saveAllCarParks(any());
    }
}
