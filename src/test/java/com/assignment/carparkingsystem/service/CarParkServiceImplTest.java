package com.assignment.carparkingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.assignment.carparkingsystem.dto.CarParkDto;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.repository.CarParkRepository;
import com.assignment.carparkingsystem.util.CoordinateDistanceCalculator;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CarParkServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CarParkServiceImplTest {

    @MockBean
    private CarParkRepository carParkRepository;
    @MockBean
    private CoordinateDistanceCalculator coordinateConvertor;
    @Autowired
    private CarParkServiceImpl carParkService;

    private List<CarPark> carParkList = new ArrayList<>();
    private CarParkQueryDto carParkQueryDto = new CarParkQueryDto();

    @BeforeEach
    void setUp() {
        carParkQueryDto = new CarParkQueryDto(1.0, 1.0, 1, 1);
        CarPark carPark = new CarPark(UUID.randomUUID(), "Test", "test", 1.0, 1.0, 1, 5,
            LocalDateTime.now(), LocalDateTime.now(), 1.0);
        carParkList.add(carPark);
    }

    @Test
    void testGetNearestCarParks_When_AllQueriesAreValidAndReturnListIsEmpty_Then_CarParkDtoList() {
        when(carParkRepository.findByAvailableLotsNot(0)).thenReturn(carParkList);
        when(coordinateConvertor.calculateHaversineDistance(anyDouble(), anyDouble(), anyDouble(),
            anyDouble())).thenReturn(1.0);
        CarParkDtoList carParkDtoList = carParkService.findNearestCarParks(carParkQueryDto);
        assertEquals(1, carParkDtoList.getCarParks().size());
        assertEquals(1, carParkDtoList.getTotalCarParks());
    }

    @Test
    void testGetNearestCarParks_When_AllQueriesAreValidAndReturnListIsEmpty_Then_EmptyCarParkDtoList() {
        when(carParkRepository.findByAvailableLotsNot(0)).thenReturn(new ArrayList<>());
        CarParkDtoList carParkDtoList = carParkService.findNearestCarParks(carParkQueryDto);
        assertEquals(0, carParkDtoList.getCarParks().size());
        assertEquals(0, carParkDtoList.getTotalCarParks());
    }

    @Test
    void testFindAllCarParks_Return_ListCarParkDto() {
        when(carParkRepository.findAll()).thenReturn(carParkList);
        List<CarParkDto> carParkDtoList = carParkService.findAllCarParks();
        assertEquals(1, carParkDtoList.size());
    }

    @Test
    void testSaveAllCarParks_When_CarParkListIsValid_Return() {
        carParkService.saveAllCarParks(carParkList);
        verify(carParkRepository, times(1)).saveAll(any());
    }

    @Test
    void testFindByCarParkNumber_When_CarParkNumberExists_ReturnCarPark() {
        when(carParkRepository.findOneByCarParkNo(any())).thenReturn(carParkList.get(0));
        carParkService.findByCarParkNumber("carParkList");
        verify(carParkRepository, times(1)).findOneByCarParkNo(any());
    }
}
